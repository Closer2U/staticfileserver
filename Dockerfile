FROM alpine:edge
ADD main /main
ADD websites /websites
RUN apk add --no-cache tzdata ca-certificates
EXPOSE 8000
CMD ["/main"]