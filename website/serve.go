package website

import (
	"encoding/json"
	"net"
	"net/http"
	"path/filepath"

	"gitlab.com/shravanshetty1/freeWebhosting/pkg"

	"github.com/dgraph-io/badger"

	"gitlab.com/shravanshetty1/centurion"
)

func Serve(c *centurion.Context, w http.ResponseWriter, r *http.Request) {
	websiteNameToData, _ := c.InjectedDependencies.Get("websiteCache").(*pkg.WebsiteCache)
	websiteName, _, _ := net.SplitHostPort(r.Host)
	websiteInformation := websiteNameToData.Get(websiteName)
	if websiteInformation != nil {
		http.StripPrefix("/", http.FileServer(http.Dir(filepath.Join(pkg.BaseDir, "websites", websiteName)))).ServeHTTP(w, r)
		return
	}
	err := c.Badger.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(websiteName))
		if err != nil {
			return err
		}
		val, err := item.ValueCopy(nil)
		if err != nil {

		}
		err = json.Unmarshal(val, &websiteInformation)
		if err != nil {
			return err
		}

		return nil
	})
	if err == badger.ErrKeyNotFound {
		c.Error(w).Generic("Could not find website " + websiteName)
		return
	}
	if err != nil {
		c.Error(w).Generic(err.Error())
	}

	if websiteInformation != nil {
		websiteNameToData.Set(websiteName, websiteInformation)
		http.StripPrefix("/", http.FileServer(http.Dir(filepath.Join(pkg.BaseDir, "websites", websiteName)))).ServeHTTP(w, r)
	}
	return
}
