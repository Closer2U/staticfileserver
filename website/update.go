package website

import (
	"archive/zip"
	"errors"
	"net/http"
	"os"
	"path/filepath"
	"unicode/utf8"

	"gitlab.com/shravanshetty1/centurion"
	"gitlab.com/shravanshetty1/freeWebhosting/pkg"

	"github.com/dgraph-io/badger"
)

func Update(c *centurion.Context, w http.ResponseWriter, r *http.Request) {
	oldWebsiteName, _ := r.Context().Value("websiteName").(string)
	newWebsiteName := r.PostFormValue("newName")
	websiteNameToData, _ := c.InjectedDependencies.Get("websiteCache").(*pkg.WebsiteCache)
	uploadedFile, fileHeader, err := r.FormFile("website")
	if err != nil {
		c.Error(w).InvalidParam("website")
		return
	}

	if fileHeader.Size > 0 {
		zippedFile, err := zip.NewReader(uploadedFile, fileHeader.Size)
		if err != nil {
			c.Error(w).InvalidParam("website")
			return
		}

		err = os.RemoveAll(filepath.Join(pkg.BaseDir, "websites", oldWebsiteName))
		if err != nil {
			c.Error(w).Generic("Could not delete website:" + oldWebsiteName + " with err:" + err.Error())
			return
		}
		err = unzipFile(zippedFile, filepath.Join(pkg.BaseDir, "websites", oldWebsiteName))
		if err != nil {
			c.Error(w).Generic("Could not unzip:" + err.Error())
			return
		}
	}

	if newWebsiteName != oldWebsiteName {
		if err := validateWebsiteName(newWebsiteName); err != nil {
			c.Error(w).InvalidParam("newName")
			return
		}

		websiteNameToData.Set(newWebsiteName, websiteNameToData.Get(oldWebsiteName))
		websiteNameToData.Delete(oldWebsiteName)

		err = os.RemoveAll(filepath.Join(pkg.BaseDir, "websites", newWebsiteName))
		if err != nil {
			c.Error(w).Generic("Could not delete website:" + newWebsiteName + " with err:" + err.Error())
			return
		}
		err = os.Rename(filepath.Join(pkg.BaseDir, "websites", oldWebsiteName), filepath.Join(pkg.BaseDir, "websites", newWebsiteName))
		if err != nil {
			c.Error(w).Generic("Could not rename website:" + err.Error())
			return
		}

		err = c.Badger.Update(func(txn *badger.Txn) error {
			i, err := txn.Get([]byte(oldWebsiteName))
			if err != nil {
				return err
			}

			oldWebsiteData, err := i.ValueCopy(nil)
			if err != nil {
				return err
			}

			err = txn.Set([]byte(newWebsiteName), oldWebsiteData)
			if err != nil {
				return err
			}

			err = txn.Delete([]byte(oldWebsiteName))
			if err != nil {
				return err
			}

			return nil
		})
		if err != nil {
			c.Error(w).Generic("Could not set to db on update:" + err.Error())
			return
		}
	}

}

func validateWebsiteName(websiteName string) error {

	if websiteName == "" {
		return errors.New("Cant give empty website name")
	}

	if len(websiteName) > 35 {
		return errors.New("WebsiteName is too long")
	}
	if !utf8.Valid([]byte(websiteName)) {
		return errors.New("Contains invalid charectors")
	}

	return nil
}
