package pkg

import (
	"sync"
	"time"

	"gitlab.com/shravanshetty1/freeWebhosting/models"
)

type WebsiteCache struct {
	mutex sync.RWMutex
	data  map[string]*models.WebsiteInformation
}

func (w *WebsiteCache) Init() {
	w.data = make(map[string]*models.WebsiteInformation)
	go func() {
		<-time.After(5 * time.Minute)
		w.mutex.Lock()
		w.data = make(map[string]*models.WebsiteInformation)
		w.mutex.Unlock()
	}()
}

func (w *WebsiteCache) Get(key string) *models.WebsiteInformation {
	w.mutex.RLock()
	defer w.mutex.RUnlock()
	return w.data[key]
}

func (w *WebsiteCache) Set(key string, value *models.WebsiteInformation) {
	w.mutex.Lock()
	w.data[key] = value
	w.mutex.Unlock()
}

func (w *WebsiteCache) Delete(key string) {
	w.mutex.Lock()
	delete(w.data, key)
	w.mutex.Unlock()
}
