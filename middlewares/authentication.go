package middlewares

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
)

var privateRoutes = map[string]string{
	"/website": "PUT",
}

func Authentication(input http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if privateRoutes[r.URL.Path] == r.Method {
			atoken, _ := r.Cookie("atoken")
			websiteName := validateJWT(atoken.Value, "asldflhadsfhlasdf@!#!@123123kkdsdkf", "websiteName", 30*time.Minute)
			if websiteName == "" {
				w.WriteHeader(400)
				w.Header().Set("Content-Type", "text/json")
				json.NewEncoder(w).Encode(struct {
					Message string `json:"message"`
				}{
					Message: "invalid access token",
				})
				return
			}
			*r = *r.WithContext(context.WithValue(r.Context(), "websiteName", websiteName))
		}
		input.ServeHTTP(w, r)
	})
}

func getHash(password string, salt string) string {
	hash := sha256.New()
	hash.Write([]byte(password))
	if salt != "" {
		hash.Write([]byte(salt))
	}
	return fmt.Sprintf("%x", hash.Sum(nil))
}

func validateJWT(aToken, tokenSalt, claimName string, expireAfter time.Duration) string {
	temp := strings.Split(aToken, ".")
	if len(temp) != 2 {
		return ""
	}

	encodedClaims := temp[0]
	hash := temp[1]
	if encodedClaims == "" || hash == "" {
		return ""
	}

	b, err := base64.StdEncoding.DecodeString(encodedClaims)
	if err != nil {
		return ""
	}

	if getHash(string(b), tokenSalt) != hash {
		return ""
	}

	claims := map[string]string{}
	err = json.Unmarshal(b, &claims)
	if err != nil {
		return ""
	}

	if timeStamp, ok := claims["timeStamp"]; ok {
		issueTime, err := time.Parse("2006-01-02 15:04:05.999999999 -0700 MST", timeStamp)
		if err != nil {
			return ""
		}
		if time.Now().After(issueTime.Add(expireAfter)) {
			return ""
		}
	}

	return claims[claimName]
}
