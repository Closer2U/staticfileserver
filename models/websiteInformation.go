package models

type WebsiteInformation struct {
	Password string `json:"password"`
	Email    string `json:"email"`
}
