package main

import (
	"gitlab.com/shravanshetty1/freeWebhosting/middlewares"

	"gitlab.com/shravanshetty1/freeWebhosting/pkg"

	"gitlab.com/shravanshetty1/centurion"
	"gitlab.com/shravanshetty1/freeWebhosting/website"

	"github.com/gorilla/mux"
)

// todo secure zip file upload and update, prevent all possible vectors of attack
// todo test and debug, write thourugh test and also, remove vuln
// todo add info logs everywhere for easier debug
// todo add domain name redirection to confirm that user owns domain name
func main() {
	ctx := centurion.New(pkg.BaseDir)

	websiteCache := &pkg.WebsiteCache{}
	websiteCache.Init()
	ctx.AddDependencies(map[string]interface{}{"websiteCache": websiteCache})

	router := mux.NewRouter()
	router.Use(middlewares.Authentication)
	router.NewRoute().Methods("GET").PathPrefix("/").Handler(ctx.Handle(website.Serve))
	router.NewRoute().Host("api.shravanshetty.net").Methods("POST").PathPrefix("/website").Handler(ctx.Handle(website.Add))
	router.NewRoute().Host("api.shravanshetty.net").Methods("PUT").PathPrefix("/website").Handler(ctx.Handle(website.Update))
	router.NewRoute().Host("api.shravanshetty.net").Methods("POST").PathPrefix("/login").Handler(ctx.Handle(website.Login))

	server := &centurion.Server{
		Router:   router,
		PortHTTP: 8000,
		//PortHTTPS:                 8001,
	}
	server.Start(ctx)
}
